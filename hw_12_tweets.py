import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

from sklearn.model_selection import train_test_split
from datasets import load_metric

import nltk
import re, string
import emoji
import nltk.data
from nltk.tokenize import sent_tokenize, word_tokenize
from nltk.stem import WordNetLemmatizer,PorterStemmer
from nltk.corpus import stopwords
stop_words = set(stopwords.words('english'))

from transformers import pipeline
from transformers import AutoTokenizer
from transformers import DataCollatorWithPadding
from transformers import AutoModelForSequenceClassification
from transformers import TrainingArguments, Trainer

from pathlib import Path  
filepath = Path('lesson12/out_tweets.csv')


tokenizer = nltk.data.load('tokenizers/punkt/PY3/english.pickle')
data_ds = pd.read_csv('lesson12/cyberbullying_tweets.csv')
input_file = 'lesson12/text.txt'


##CUSTOM DEFINED FUNCTIONS TO CLEAN THE TWEETS

#Clean emojis from text
def strip_emoji(text):
    return re.sub(emoji.get_emoji_regexp(), r"", text) #remove emoji

#Remove punctuations, links, stopwords, mentions and \r\n new line characters
def strip_all_entities(text): 
    text = text.replace('\r', '').replace('\n', ' ').lower() #remove \n and \r and lowercase
    text = re.sub(r"(?:\@|https?\://)\S+", "", text) #remove links and mentions
    text = re.sub(r'[^\x00-\x7f]',r'', text) #remove non utf8/ascii characters such as '\x9a\x91\x97\x9a\x97'
    banned_list= string.punctuation
    table = str.maketrans('', '', banned_list)
    text = text.translate(table)
    text = [word for word in text.split() if word not in stop_words]
    text = ' '.join(text)
    text =' '.join(word for word in text.split() if len(word) < 14) # remove words longer than 14 characters
    return text

#remove contractions
def decontract(text):
    text = re.sub(r"can\'t", "can not", text)
    text = re.sub(r"n\'t", " not", text)
    text = re.sub(r"\'re", " are", text)
    text = re.sub(r"\'s", " is", text)
    text = re.sub(r"\'d", " would", text)
    text = re.sub(r"\'ll", " will", text)
    text = re.sub(r"\'t", " not", text)
    text = re.sub(r"\'ve", " have", text)
    text = re.sub(r"\'m", " am", text)
    return text

#clean hashtags at the end of the sentence, and keep those in the middle of the sentence by removing just the "#" symbol
def clean_hashtags(tweet):
    new_tweet = " ".join(word.strip() for word in re.split('#(?!(?:hashtag)\b)[\w-]+(?=(?:\s+#[\w-]+)*\s*$)', tweet)) #remove last hashtags
    new_tweet2 = " ".join(word.strip() for word in re.split('#|_', new_tweet)) #remove hashtags symbol from words in the middle of the sentence
    return new_tweet2

#Filter special characters such as "&" and "$" present in some words
def filter_chars(a):
    sent = []
    for word in a.split(' '):
        if ('$' in word) | ('&' in word):
            sent.append('')
        else:
            sent.append(word)
    return ' '.join(sent)

#Remove multiple sequential spaces
def remove_mult_spaces(text):
    return re.sub("\s\s+" , " ", text)

#Stemming
def stemmer(text):
    tokenized = nltk.word_tokenize(text)
    ps = PorterStemmer()
    return ' '.join([ps.stem(words) for words in tokenized])

#Lemmatization 
#NOTE:Stemming seems to work better for this dataset
def lemmatize(text):
    tokenized = nltk.word_tokenize(text)
    lm = WordNetLemmatizer()
    return ' '.join([lm.lemmatize(words) for words in tokenized])

#Then we apply all the defined functions in the following order
def deep_clean(text):
   #  text = strip_emoji(text)
    text = decontract(text)
    text = strip_all_entities(text)
    text = clean_hashtags(text)
    text = filter_chars(text)
    text = remove_mult_spaces(text)
    text = stemmer(text)
    return text



############################

# Data Import

data_ds = data_ds.rename(columns={'tweet_text': 'text', 'cyberbullying_type': 'sentiment'})

print(data_ds.head())

print(data_ds.info())

# Are there duplicated tweets?

print(data_ds.duplicated().sum())
data_ds = data_ds[~data_ds.duplicated()]

print(data_ds.info())

# Are the classes balanced?

print(data_ds.sentiment.value_counts())

# Tweets text deep cleaning

texts_new = []
for t in data_ds.text:
    texts_new.append(deep_clean(t))
data_ds['text_clean'] = texts_new
print(data_ds.head())

# Are there duplicate tweets after the cleaning?

print(data_ds.shape)
data_ds["text_clean"].duplicated().sum()
data_ds.drop_duplicates("text_clean", inplace=True)
print(data_ds.shape)

print(data_ds.sentiment.value_counts())

data_ds = data_ds[data_ds["sentiment"]!="other_cyberbullying"]

sentiments = ["religion","age","ethnicity","gender","not bullying"]

# Tweets length analysis

text_len = []
for text in data_ds.text_clean:
    tweet_len = len(text.split())
    text_len.append(tweet_len)


data_ds['text_len'] = text_len

data_ds = data_ds[data_ds['text_len'] > 3]

# What about long tweets?

data_ds.sort_values(by=['text_len'], ascending=False)

data_ds = data_ds[data_ds['text_len'] < 100]

max_len = np.max(data_ds['text_len'])
print(max_len)

data_ds.sort_values(by=["text_len"], ascending=False)
 
# Sentiment column encoding

data_ds['sentiment'] = data_ds['sentiment'].replace({'religion':0,'age':1,'ethnicity':2,'gender':3,'not_cyberbullying':4})


print(type(data_ds['text']))


print(data_ds.info())

#######################

data_ds = data_ds[0:150]
print(data_ds.info())

#######################

base = pipeline("sentiment-analysis")

data_ds['base'] = data_ds['text_clean'].apply(lambda x: pd.Series(base(x)))

data_ds = pd.concat([data_ds.drop(['base'], axis=1), data_ds['base'].apply(pd.Series)], axis=1)

print(data_ds.head())


data_ds = data_ds.rename(columns={
   'label':'base_label',
   'score':'base_score'
   })

print(data_ds.head())

top_sent_base = data_ds["base_label"].describe()["top"]

print('The most common mood is: ', top_sent_base)


#######################

roberta = pipeline(model="cardiffnlp/twitter-roberta-base-sentiment")

data_ds['roberta'] = data_ds['text_clean'].apply(lambda x: pd.Series(roberta(x)))

data_ds = pd.concat([data_ds.drop(['roberta'], axis=1), data_ds['roberta'].apply(pd.Series)], axis=1)

print(data_ds.head())

data_ds = data_ds.rename(columns={
   'label':'roberta_label',
   'score':'roberta_score'
   })

data_ds['roberta_label'] = data_ds['roberta_label'].replace([
   'LABEL_0',
   'LABEL_1',
   'LABEL_2'], [
      'NEGATIVE',
      'NEUTRAL',
      'POSITIVE'
      ])

print(data_ds.head())

top_sent_roberta = data_ds["roberta_label"].describe()["top"]

print('The most common sentiment is: ', top_sent_roberta)

#######################

multilingual = pipeline(model="nlptown/bert-base-multilingual-uncased-sentiment")

data_ds['multilingual'] = data_ds['text_clean'].apply(lambda x: pd.Series(multilingual(x)))

data_ds = pd.concat([data_ds.drop(['multilingual'], axis=1), data_ds['multilingual'].apply(pd.Series)], axis=1)

print(data_ds.head())

data_ds = data_ds.rename(columns={
   'label':'multilingual_label',
   'score':'multilingual_score'
   })

print(data_ds.head())

top_sent_multilingual = data_ds["multilingual_label"].describe()["top"]

print('The most common sentiment is: ', top_sent_multilingual)

#######################

distilbert = pipeline(model="bhadresh-savani/distilbert-base-uncased-emotion")

data_ds['distilbert'] = data_ds['text_clean'].apply(lambda x: pd.Series(distilbert(x)))

data_ds = pd.concat([data_ds.drop(['distilbert'], axis=1), data_ds['distilbert'].apply(pd.Series)], axis=1)

print(data_ds.head())

data_ds = data_ds.rename(columns={
   'label':'distilbert_label',
   'score':'distilbert_score'
   })

print(data_ds.head())

top_sent_distilbert = data_ds["distilbert_label"].describe()["top"]

print('The most common sentiment is: ', top_sent_distilbert)

#######################

# Вызов модели со своего аккаунта

# imdb = pipeline(model="federicopascual/finetuning-sentiment-model-3000-samples")
imdb = pipeline(model="MaksymK/hillel-finetuning-sentiment-model")

data_ds['imdb'] = data_ds['text_clean'].apply(lambda x: pd.Series(imdb(x)))

data_ds = pd.concat([data_ds.drop(['imdb'], axis=1), data_ds['imdb'].apply(pd.Series)], axis=1)

print(data_ds.head())

data_ds = data_ds.rename(columns={
   'label':'imdb_label',
   'score':'imdb_score'
   })

data_ds['imdb_label'] = data_ds['imdb_label'].replace([
   'LABEL_0',
   'LABEL_1'], [
      'NEGATIVE',
      'POSITIVE'
      ])

print(data_ds.head())

top_sent_imdb = data_ds["imdb_label"].describe()["top"]

print('The most common sentiment is: ', top_sent_imdb)

#######################

sns.countplot(data=data_ds, x="base_label")
plt.title('Distribution of sentiments (base) in the dataset (tweets)')
plt.savefig('lesson12/figures/tweets-1.png')
plt.show()

sns.countplot(data=data_ds, x="roberta_label")
plt.title('Distribution of sentiments (roberta) in the dataset (tweets)')
plt.savefig('lesson12/figures/tweets-2.png')
plt.show()

sns.countplot(data=data_ds, x="multilingual_label")
plt.title('Distribution of sentiments (multilingual) in the dataset (tweets)')
plt.savefig('lesson12/figures/tweets-3.png')
plt.show()

sns.countplot(data=data_ds, x="distilbert_label")
plt.title('Distribution of sentiments (distilbert) in the dataset (tweets)')
plt.savefig('lesson12/figures/tweets-4.png')
plt.show()

sns.countplot(data=data_ds, x="imdb_label")
plt.title('Distribution of sentiments (imdb) in the dataset (tweets)')
plt.savefig('lesson12/figures/tweets-5.png')
plt.show()

x = np.arange(len(data_ds["base_score"]))

sns.lineplot(data=data_ds, x = x, y="base_score")
plt.title('Plot of score values depending on the model (base) and dataset (tweets)')
plt.savefig('lesson12/figures/tweets-1-1.png')
plt.show()

sns.lineplot(data=data_ds, x = x, y="roberta_score")
plt.title('Plot of score values depending on the model (roberta) and dataset (tweets)')
plt.savefig('lesson12/figures/tweets-2-1.png')
plt.show()

sns.lineplot(data=data_ds, x = x, y="multilingual_score")
plt.title('Plot of score values depending on the model (multilingual) and dataset (tweets)')
plt.savefig('lesson12/figures/tweets-3-1.png')
plt.show()

sns.lineplot(data=data_ds, x = x, y="distilbert_score")
plt.title('Plot of score values depending on the model (distilbert) and dataset (tweets)')
plt.savefig('lesson12/figures/tweets-4-1.png')
plt.show()

sns.lineplot(data=data_ds, x = x, y="imdb_score")
plt.title('Plot of score values depending on the model (imdb) and dataset (tweets)')
plt.savefig('lesson12/figures/tweets-5-1.png')
plt.show()

#######################

data_ds[0:15].to_csv(filepath)

#######################

mean_sent_base = data_ds["base_score"].describe()["mean"]
mean_sent_roberta = data_ds["roberta_score"].describe()["mean"]
mean_sent_multilingual = data_ds["multilingual_score"].describe()["mean"]
mean_sent_imdb = data_ds["imdb_score"].describe()["mean"]
mean_sent_distilbert = data_ds["distilbert_score"].describe()["mean"]

print("The mean value of the sentiment score (base): ", mean_sent_base)
print("The mean value of the sentiment score (roberta): ", mean_sent_roberta)
print("The mean value of the sentiment score (multilingual): ", mean_sent_multilingual)
print("The mean value of the sentiment score (distilbert): ", mean_sent_distilbert)
print("The mean value of the sentiment score (imdb): ", mean_sent_imdb)

list_scores = [mean_sent_base, mean_sent_roberta, mean_sent_multilingual, mean_sent_distilbert, mean_sent_imdb]
list_models = ["base", "roberta", "multilingual", "distilbert", "imdb"]

fin_df = pd.DataFrame(list(zip(list_models, list_scores)),
               columns =['models', 'scores'])

print(fin_df)

sns.barplot(data=fin_df, x="models", y="scores")
plt.ylim(0.45)
plt.title('The mean value of the sentiment score of the text, depending on the model and data set (tweets)')
plt.savefig('lesson12/figures/mean-tweets.png')
plt.show()