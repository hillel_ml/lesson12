import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

from sklearn.model_selection import train_test_split
from datasets import load_metric

import nltk
import re, string
import emoji
import nltk.data
from nltk.tokenize import sent_tokenize, word_tokenize
from nltk.stem import WordNetLemmatizer,PorterStemmer
from nltk.corpus import stopwords
stop_words = set(stopwords.words('english'))

from transformers import pipeline
from transformers import AutoTokenizer
from transformers import DataCollatorWithPadding
from transformers import AutoModelForSequenceClassification
from transformers import TrainingArguments, Trainer
from transformers import AutoModel


from pathlib import Path  
filepath = Path('lesson12/out_text.csv')



tokenizer = nltk.data.load('tokenizers/punkt/PY3/english.pickle')
data_ds = pd.read_csv('lesson12/cyberbullying_tweets.csv')
input_file = 'lesson12/text.txt'


##CUSTOM DEFINED FUNCTIONS TO CLEAN THE TWEETS

#Clean emojis from text
def strip_emoji(text):
    return re.sub(emoji.get_emoji_regexp(), r"", text) #remove emoji

#Remove punctuations, links, stopwords, mentions and \r\n new line characters
def strip_all_entities(text): 
    text = text.replace('\r', '').replace('\n', ' ').lower() #remove \n and \r and lowercase
    text = re.sub(r"(?:\@|https?\://)\S+", "", text) #remove links and mentions
    text = re.sub(r'[^\x00-\x7f]',r'', text) #remove non utf8/ascii characters such as '\x9a\x91\x97\x9a\x97'
    banned_list= string.punctuation
    table = str.maketrans('', '', banned_list)
    text = text.translate(table)
    text = [word for word in text.split() if word not in stop_words]
    text = ' '.join(text)
    text =' '.join(word for word in text.split() if len(word) < 14) # remove words longer than 14 characters
    return text

#remove contractions
def decontract(text):
    text = re.sub(r"can\'t", "can not", text)
    text = re.sub(r"n\'t", " not", text)
    text = re.sub(r"\'re", " are", text)
    text = re.sub(r"\'s", " is", text)
    text = re.sub(r"\'d", " would", text)
    text = re.sub(r"\'ll", " will", text)
    text = re.sub(r"\'t", " not", text)
    text = re.sub(r"\'ve", " have", text)
    text = re.sub(r"\'m", " am", text)
    return text

#clean hashtags at the end of the sentence, and keep those in the middle of the sentence by removing just the "#" symbol
def clean_hashtags(tweet):
    new_tweet = " ".join(word.strip() for word in re.split('#(?!(?:hashtag)\b)[\w-]+(?=(?:\s+#[\w-]+)*\s*$)', tweet)) #remove last hashtags
    new_tweet2 = " ".join(word.strip() for word in re.split('#|_', new_tweet)) #remove hashtags symbol from words in the middle of the sentence
    return new_tweet2

#Filter special characters such as "&" and "$" present in some words
def filter_chars(a):
    sent = []
    for word in a.split(' '):
        if ('$' in word) | ('&' in word):
            sent.append('')
        else:
            sent.append(word)
    return ' '.join(sent)

#Remove multiple sequential spaces
def remove_mult_spaces(text):
    return re.sub("\s\s+" , " ", text)

#Stemming
def stemmer(text):
    tokenized = nltk.word_tokenize(text)
    ps = PorterStemmer()
    return ' '.join([ps.stem(words) for words in tokenized])

#Lemmatization 
#NOTE:Stemming seems to work better for this dataset
def lemmatize(text):
    tokenized = nltk.word_tokenize(text)
    lm = WordNetLemmatizer()
    return ' '.join([lm.lemmatize(words) for words in tokenized])

#Then we apply all the defined functions in the following order
def deep_clean(text):
   #  text = strip_emoji(text)
    text = decontract(text)
    text = strip_all_entities(text)
    text = clean_hashtags(text)
    text = filter_chars(text)
    text = remove_mult_spaces(text)
    text = stemmer(text)
    return text





# extract the input as a stream of words
print("Extracting text from input...")
fin = open(input_file, 'rb')
lines = []
for line in fin:
    line = line.strip().lower()
    line = line.decode("ascii", "ignore")
    if len(line) == 0:
        continue
    lines.append(line)
fin.close()
raw_data = " ".join(lines)


############################
tokenized_text = tokenizer.tokenize(raw_data)
df = pd.DataFrame (tokenized_text, columns = ['text_sent'])
print(df.head())

text_clean = []
for t in df.text_sent:
    text_clean.append(deep_clean(t))
df['text_clean'] = text_clean
print(df.head())
############################

# df = df[0:5]
# print(df.info())


base = pipeline("sentiment-analysis")

df['base'] = df['text_clean'].apply(lambda x: pd.Series(base(x)))

df = pd.concat([df.drop(['base'], axis=1), df['base'].apply(pd.Series)], axis=1)

print(df.head())

df = df.rename(columns={
   'label':'base_label',
   'score':'base_score'
   })

print(df.head())

top_sent_base = df["base_label"].describe()["top"]

print('The most common sentiment is: ', top_sent_base)

#######################

roberta = pipeline(model="cardiffnlp/twitter-roberta-base-sentiment")

df['roberta'] = df['text_clean'].apply(lambda x: pd.Series(roberta(x)))

df = pd.concat([df.drop(['roberta'], axis=1), df['roberta'].apply(pd.Series)], axis=1)

print(df.head())

df = df.rename(columns={
   'label':'roberta_label',
   'score':'roberta_score'
   })

df['roberta_label'] = df['roberta_label'].replace([
   'LABEL_0',
   'LABEL_1',
   'LABEL_2'], [
      'NEGATIVE',
      'NEUTRAL',
      'POSITIVE'
      ])

print(df.head())

top_sent_roberta = df["roberta_label"].describe()["top"]

print('The most common sentiment is: ', top_sent_roberta)

#######################

multilingual = pipeline(model="nlptown/bert-base-multilingual-uncased-sentiment")

df['multilingual'] = df['text_clean'].apply(lambda x: pd.Series(multilingual(x)))

df = pd.concat([df.drop(['multilingual'], axis=1), df['multilingual'].apply(pd.Series)], axis=1)

print(df.head())

df = df.rename(columns={
   'label':'multilingual_label',
   'score':'multilingual_score'
   })

print(df.head())

top_sent_multilingual = df["multilingual_label"].describe()["top"]

print('The most common sentiment is: ', top_sent_multilingual)

#######################

distilbert = pipeline(model="bhadresh-savani/distilbert-base-uncased-emotion")

df['distilbert'] = df['text_clean'].apply(lambda x: pd.Series(distilbert(x)))

df = pd.concat([df.drop(['distilbert'], axis=1), df['distilbert'].apply(pd.Series)], axis=1)

print(df.head())

df = df.rename(columns={
   'label':'distilbert_label',
   'score':'distilbert_score'
   })

print(df.head())

top_sent_distilbert = df["distilbert_label"].describe()["top"]

print('The most common sentiment is: ', top_sent_distilbert)

#######################

# Локальный вызов модели

# path = "/home/maksym/finetuning_sentiment/checkpoint-376/"
# tokenizer_imdb = AutoTokenizer.from_pretrained(path)
# model_imdb = AutoModel.from_pretrained(path)

# test_input = tokenizer_imdb(["I love this move", "This movie sucks!"])
# test_output = model_imdb(test_input)

# print(test_output)

#####################################

# Вызов модели со своего аккаунта

# imdb = pipeline(model="federicopascual/finetuning-sentiment-model-3000-samples")
imdb = pipeline(model="MaksymK/hillel-finetuning-sentiment-model")

df['imdb'] = df['text_clean'].apply(lambda x: pd.Series(imdb(x)))

df = pd.concat([df.drop(['imdb'], axis=1), df['imdb'].apply(pd.Series)], axis=1)

print(df.head())

df = df.rename(columns={
   'label':'imdb_label',
   'score':'imdb_score'
   })

df['imdb_label'] = df['imdb_label'].replace([
   'LABEL_0',
   'LABEL_1'], [
      'NEGATIVE',
      'POSITIVE'
      ])

print(df.head())

top_sent_imdb = df["imdb_label"].describe()["top"]

print('The most common sentiment is: ', top_sent_imdb)

#######################


sns.countplot(data=df, x="base_label")
plt.title('Distribution of sentiments (base) in the dataset (text)')
plt.savefig('lesson12/figures/text-1.png')
plt.show()

sns.countplot(data=df, x="roberta_label")
plt.title('Distribution of sentiments (roberta) in the dataset (text)')
plt.savefig('lesson12/figures/text-2.png')
plt.show()

sns.countplot(data=df, x="multilingual_label")
plt.title('Distribution of sentiments (multilingual) in the dataset (text)')
plt.savefig('lesson12/figures/text-3.png')
plt.show()

sns.countplot(data=df, x="distilbert_label")
plt.title('Distribution of sentiments (distilbert) in the dataset (text)')
plt.savefig('lesson12/figures/text-4.png')
plt.show()

sns.countplot(data=df, x="imdb_label")
plt.title('Distribution of sentiments (imdb) in the dataset (text)')
plt.savefig('lesson12/figures/text-5.png')
plt.show()

x = np.arange(len(df["base_score"]))

sns.lineplot(data=df, x = x, y="base_score")
plt.title('Plot of score values depending on the model (base) and dataset (text)')
plt.savefig('lesson12/figures/text-1-1.png')
plt.show()

sns.lineplot(data=df, x = x, y="roberta_score")
plt.title('Plot of score values depending on the model (roberta) and dataset (text)')
plt.savefig('lesson12/figures/text-2-1.png')
plt.show()

sns.lineplot(data=df, x = x, y="multilingual_score")
plt.title('Plot of score values depending on the model (multilingual) and dataset (text)')
plt.savefig('lesson12/figures/text-3-1.png')
plt.show()

sns.lineplot(data=df, x = x, y="distilbert_score")
plt.title('Plot of score values depending on the model (distilbert) and dataset (text)')
plt.savefig('lesson12/figures/text-4-1.png')
plt.show()

sns.lineplot(data=df, x = x, y="imdb_score")
plt.title('Plot of score values depending on the model (imdb) and dataset (text)')
plt.savefig('lesson12/figures/text-5-1.png')
plt.show()

#################################

df[0:15].to_csv(filepath)

#################################

mean_sent_base = df["base_score"].describe()["mean"]
mean_sent_roberta = df["roberta_score"].describe()["mean"]
mean_sent_multilingual = df["multilingual_score"].describe()["mean"]
mean_sent_imdb = df["imdb_score"].describe()["mean"]
mean_sent_distilbert = df["distilbert_score"].describe()["mean"]

print("The mean value of the sentiment score (base): ", mean_sent_base)
print("The mean value of the sentiment score (roberta): ", mean_sent_roberta)
print("The mean value of the sentiment score (multilingual): ", mean_sent_multilingual)
print("The mean value of the sentiment score (distilbert): ", mean_sent_distilbert)
print("The mean value of the sentiment score (imdb): ", mean_sent_imdb)

list_scores = [mean_sent_base, mean_sent_roberta, mean_sent_multilingual, mean_sent_distilbert, mean_sent_imdb]
list_models = ["base", "roberta", "multilingual", "distilbert", "imdb"]

fin_df = pd.DataFrame(list(zip(list_models, list_scores)),
               columns =['models', 'scores'])

print(fin_df)

sns.barplot(data=fin_df, x="models", y="scores")
plt.ylim(0.4)
plt.title('The mean value of the sentiment score of the text, depending on the model and data set (text)')
plt.savefig('lesson12/figures/mean-text.png')
plt.show()